public static void factorial(){
        String dato= JOptionPane.showInputDialog("Introduce un numero para obtener su factorial"); 
        double factorial=1, numero = Double.parseDouble(dato);
        //Solución con while
        while ( numero!=0) 
        {
          factorial=factorial*numero;
          numero--;
        }
        //Solución con for
        for (int x=2;x<=numero;x++)
            factorial = factorial * x;
       
        System.out.println(factorial);
        }
