/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 *
 * @author Programacion
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void operando() {
        // Ejercicio molón: 1) Introducir 2 números por teclado y luego un carácter indicando la operación a realizar (+,-,*,/) y devolver el resultado de la operación, utilizando un switch
         double x, y, resultado;
        String op1=JOptionPane.showInputDialog("Introduce un número");
        x=Double.parseDouble(op1);
        String op2=JOptionPane.showInputDialog("Introduce otro número");
        y=Double.parseDouble(op2);
        String operador=JOptionPane.showInputDialog("Introduce el operador a utilizar");
        switch(operador)
        {
              case "-":
                             resultado = x-y;
                             System.out.println("La resta es: " + resultado);
                             break;
              case "+":
                             resultado = x+y;
                             System.out.println("La suma es: " + resultado);
                             break;
              case "*":
                             resultado = x*y;
                             System.out.println("La multiplicación es: " + resultado);
                             break;
              case "/":
                             if(y!=0)
                             {
                                    resultado = x/y;
                                   System.out.println("La división es: " + resultado);
                             }
                             else
                                   System.out.println("no se puede dividir entre cero");
                             break;
              default:
                            System.out.println("Algún dato introducido es erróneo");
        }
    }
